O(Objective):
+ Today we learned about Redux, a state container that helps us conveniently manage various states in a project.
+ The Code Review in the morning solved a question I had about my homework yesterday. When a function is passed in parentheses, JavaScript will directly call it.
+ Practicing using Redux to refactor yesterday's Counter and Todo-List projects has helped me better grasp the knowledge I learned today, but there are still some areas where I am not very proficient.
+ Completed the Redux replacement of the Todo-List project and added functions for deletion and modification.
+ In the afternoon, I learned the testing programming form of the JS front-end and conducted simple exercises using the Counter project.

R(Reflective):
+ Fruitful

I(Interpretive):
+ I am not very familiar with some syntax of JS and some usage rules of Redux, which requires further learning. 
+ I am not familiar with some properties of CSS styles in today's project and need further practice.

D(Decision):
+ Continue practicing and consolidating the knowledge learned today.
