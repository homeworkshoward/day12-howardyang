import { configureStore } from '@reduxjs/toolkit'
import todoSlice from '../Components/todoSlice';

export default configureStore({
  reducer: {
    todo: todoSlice,
  },
});