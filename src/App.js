import './App.css';
import { TodoList } from "./Components/TodoList";
import './Components/TodoList.css'
function App() {
  return (

    <div className='TodoList'>
      <h1>Todo List</h1>
      <TodoList></TodoList>
    </div>
    
  );

}

export default App;
