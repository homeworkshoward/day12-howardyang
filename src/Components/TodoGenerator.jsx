import { useState } from "react"
import { useDispatch } from "react-redux";
import { addTodo } from "./todoSlice";

export function TodoGenerator() {
    const dispatch = useDispatch();
    const [text, setText] = useState('');

    const changeText = (inputText) => {
        setText(inputText)
    }

    function addInputTodo() {

        dispatch(addTodo({
            id: new Date().toString(),
            text: text,
            done: false
        }))

        setText("");
    }

    return (
        <div>
            <input type="text" value={text} onInput={(inputText) => changeText(inputText.target.value)}></input>
            <button onClick={addInputTodo}>Add</button>
        </div>
    )
}