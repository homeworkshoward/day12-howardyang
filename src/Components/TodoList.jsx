import { TodoGroup } from "./TodoGroup"
import { TodoGenerator } from "./TodoGenerator"
import {useSelector} from "react-redux";
export function TodoList() {
  const todoList = useSelector((state)=> state.todo.todoList)

  return <>
    <TodoGroup todoList={todoList}></TodoGroup>
    <TodoGenerator></TodoGenerator>
  </>

}