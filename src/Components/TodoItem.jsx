// import { useState } from "react";
import { useDispatch } from "react-redux";
import { deleteTodo, setDone } from "./todoSlice";
import "./TodoItem.css"
export const TodoItem = (props) => {
    const dispatch = useDispatch();

    const setTodoDone = () => {
        dispatch(setDone(props.todo.id))
    }

    const deleteTodoItem = () => {
        dispatch(deleteTodo(props.todo.id))
    }

    return (
        <div className="todoItem">
            <span onClick={setTodoDone} className={props.todo.done ? "todoItemText" : null}>{props.todo.text}</span>
            <span onClick={deleteTodoItem} className="X">X</span>
        </div>
        
    )
}

