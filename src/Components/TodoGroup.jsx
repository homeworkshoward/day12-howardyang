import { TodoItem } from "./TodoItem"

export function TodoGroup(props) {

    return <>
        {props.todoList.map((todo, index) => (
            <TodoItem key={index} todo={todo}></TodoItem>
        ))}
    </>;
}