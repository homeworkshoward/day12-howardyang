import { createSlice } from '@reduxjs/toolkit'

export const todoSlice = createSlice({
  name: 'todo',
  initialState: {
    todoList: []
  },
  reducers: {
    addTodo: (state, action) => {
      const todo = action.payload;
      state.todoList = state.todoList.concat(todo);
    },

    setDone: (state, action) => {
      const todoId = action.payload;
      const updateList = state.todoList.map(todo => {
        if (todo.id === todoId) {
          return {
            ...todo,
            done: !todo.done
          }
        }
        return todo
      })
      state.todoList = updateList
    },

    deleteTodo: (state, action) => {
      const todoId = action.payload;
      state.todoList = state.todoList.filter(todo => {
        return todo.id !== todoId;
      })

    }

  }
})

export const { addTodo, setDone, deleteTodo } = todoSlice.actions;

export default todoSlice.reducer