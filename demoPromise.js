function read(number) {
    console.log("demo: "+ number);
	
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            if(number > 4){
                resolve(number)
            }else{
                reject(number)
            }
        }, 1000 * number)
    })
}

const result = Promise.all([read(5), read(2)]);
result.then((data) => {
    console.log('成功'+ data)
},(err)=>{
    console.log('失败'+ err)
})
